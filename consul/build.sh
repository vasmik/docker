#!/usr/bin/env bash

docker build -t vasmik/consul .
docker build -t vasmik/consul-agent ./agent
docker build -t vasmik/consul-server ./server